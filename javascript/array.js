/*
    1. An array is a special variable, which can hold more than one value at a time.
    2. An array can hold many values under a single name, and you can access the values by referring to an index number.
*/
var cars = ["Saab", "Volvo", "BMW"];
/*
    The following example also creates an Array, and assigns values to it:
*/
var cars1 = new Array("Saab", "Volvo", "BMW");
/*
   The two examples above do exactly the same. There is no need to use new Array().
   For simplicity, readability and execution speed, use the first one (the array literal method).

   You access an array element by referring to the index number.
   This statement accesses the value of the first element in cars

   Note: Array indexes start with 0.
 */
console.log(cars[0]);
cars[0] = "Maruti";
console.log(cars);
/* The real strength of JavaScript arrays are the built-in array properties and methods */

console.log("array lenght is =" + cars.length);
console.log(cars.sort());
//dynamic list
var text;
text = "<ul>";
for (let i = 0; i < cars.length; i++) {
    console.log(cars[i]);
    text += "<li>" + cars[i] + "</li>";

}
text += "</ul>";
document.getElementById("car").innerHTML = text;

//dynamic table using Array.forEach method
var table;
table = "<table border='2'>";
table += "<tr>";
table += "<th> Car Name </th>";
table += "</tr>";

cars.forEach(function (car) {
    console.log(car);
    table += "<tr>"
    table += "<td>" + car + "</td>";
    table += "</tr>"
});

table += "</table>";
document.getElementById("car").innerHTML = table;

console.log(typeof cars);
console.log(cars instanceof Array);
console.log(cars instanceof Object);
console.log(cars instanceof String);