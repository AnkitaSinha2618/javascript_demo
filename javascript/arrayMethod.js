var fruits = ["Banana", "Orange", "Apple", "Mango"];
document.getElementById("demo").innerHTML = fruits.toString();
//The length property provides an easy way to append a new element to an array:
//length is always greater than last index of an array 
fruits[fruits.length] = "Kiwi";
console.log(fruits);
// /Using delete may leave undefined holes in the array. Use pop() or shift() instead
// delete fruits[0];
// console.log(fruits);
/**
 * Splicing an Array
 * 
 * The splice() method can be used to add new items to an array: 

*/
fruits.splice(2, 1, "Lemon",);
console.log(fruits);
//With clever parameter setting, you can use splice() to remove elements without leaving "holes" in the array: 
fruits.splice(0, 1);
console.log(fruits);

// MAP FUNCTION

//without map
var numbers = [4, 9, 16, 25];
var num = numbers;
console.log(numbers);
num.pop();

console.log(numbers);
console.log(num);
// With MAP FUNCTION
console.log("********");
var numbers1 = [4, 9, 16, 25];
var num1 = numbers1.map(function (num) { return num; });
console.log(numbers1);
num1.pop();

console.log(numbers1);
console.log(num1);

// Array Sorting 

var age = [5, 9, 8, 6, 2, 4, 1, 0, 3, 5];
//default sorting 
console.log(age.reverse());
//Sort an array alphabetically, and then reverse the order of the sorted items (descending):
console.log(age.sort());
console.log(age.reverse());

//
var nums = ["25", "100", "75"];
//console.log(nums.sort());
//customize sorting 

//it will sort in acsending order
var newAge1 = age.sort(function (a, b) {
    return a - b;
});
//console.log(newAge1);

//it will sort in descending order
var newAge2 = age.sort(function (a, b) {
    return b - a;
});
//console.log(newAge2);

////it will sort in acsending order
var newNums = nums.sort(function (a, b) {
    return a - b;
});
//console.log(newNums);

// Sorting Object type array

var persons = [{ "name": "B", "age": 10 }, { "name": "A", "age": 50 }, { "name": "C", "age": 100 }, { "name": "D", "age": 30 }];
console.log(persons);
//sort person by name acsending order
// console.log(persons.sort(function (a, b) {
//     if (a.name < b.name) {
//         return -1;
//     } else if (a.name > b.name) {
//         return 1;
//     }
//     return 0;
// }));

//sort person by age descending order
console.log(persons.sort(function (a, b) {
    return b.age - a.age;
}));